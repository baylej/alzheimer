// Displays a texture fullscreen.
function Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc) {

	if (Displayable.prototype.mesh == null) {

		var vertices = [ // [4][3]
			-1, -1, 0,    1, -1, 0,    1,  1, 0,   -1,  1, 0,
			-1, -1, 0,    1, -1, 0,    1,  1, 0,   -1,  1, 0,
		];

		var colors = [ // [4][3]
			 1,  0, 0,    1,  0, 0,    1,  0, 0,    1,  0, 0,
		];

		var uvs = [ // [4][2]
			0., 1.,    1., 1.,    1., 0.,    0., 0.,
		];

		var indices = [ // [2][3]
			 0,  1,  2,   0,  2,  3,
		];

		var hastx = [ // [4][1]
			1., 1., 1., 1.,
		];

		Displayable.prototype.mesh = new IndexedMesh(indices, vertices, colors, uvs, hastx);
	}

	this.tex_loc = texloc;
	this.tex_slot_id = texslotid;

	this.modelmat_loc = modelmatloc;
	this.texmat_loc = texmatloc;
	this.projmat_loc = projmatloc;
	this.viewmat_loc = viewmatloc;

	this.modelmat = mat4.identity(mat4.create());
	this.texmat   = mat3.identity(mat3.create());

	this.exec = function() {
		gl.uniform1i(this.tex_loc, this.tex_slot_id);
		var identm = mat4.identity(mat4.create());
		gl.uniformMatrix4fv(this.projmat_loc, gl.FALSE, identm);
		gl.uniformMatrix4fv(this.viewmat_loc, gl.FALSE, identm);
		gl.uniformMatrix4fv(this.modelmat_loc, gl.FALSE, this.modelmat);
		gl.uniformMatrix3fv(this.texmat_loc, gl.FALSE, this.texmat);
		Displayable.prototype.mesh.draw();
	};
}
Displayable.prototype.mesh = null;
// To draw a subset of this Displayable
Displayable.prototype.sourceRegion = function(pxt, left, top, w, h) {
	var utim = mat3.create();
	mat3.fromTranslation(utim, vec2.fromValues(left*pxt, top*pxt));
	mat3.fromScaling(this.texmat, vec2.fromValues(w*pxt, h*pxt));
	mat3.multiply(this.texmat, utim, this.texmat);
}
// To set the target with a scale
Displayable.prototype.targetScaled = function(pxs, left, top, w, h) {
	var utim = mat4.create();
	mat4.fromTranslation(utim, vec3.fromValues(left*pxs, top*pxs, 0.));
	mat4.fromScaling(this.modelmat, vec3.fromValues(w/2.*pxs, h/2.*pxs, 1.));
	mat4.multiply(this.modelmat, utim, this.modelmat);
}


// Adds a timeout and skipable state to a Displayable
function TimedSkippableDisplayable(displayable, timeout, ctx_stack_to_pop) {
	this.display = displayable;
	this.ctxstack = ctx_stack_to_pop;
	this.timeout = timeout;
	this.end = Date.now() + timeout;

	this.exec = function() {
		if (Date.now() > this.end || ev_stack.length > 0) {
			ev_stack.length=0;
			this.ctxstack.pop();
			log('timedskippableDisplayable ending...');
		}
		this.display.exec();
	};
}
TimedSkippableDisplayable.prototype.reset = function() {
	this.end = Date.now() + this.timeout;
}
