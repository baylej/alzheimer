// Pixel size (tex coordinates)
var pxt = .03125; // 1/32
// Pixel size (screen coordinates)
var pxs = .03125; // 2/64

// Spritesheet
function SpriteSheet(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc) {
	var utim = mat3.create();
	// "0" (zero) glyph
	this.c0 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c0.sourceRegion(pxt, 0, 0, 3, 6);

	this.c1 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c1.sourceRegion(pxt, 4, 0, 3, 6);

	this.c2 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c2.sourceRegion(pxt, 8, 0, 3, 6);

	this.c3 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c3.sourceRegion(pxt, 12, 0, 3, 6);

	this.c4 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c4.sourceRegion(pxt, 16, 0, 3, 6);

	this.c5 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c5.sourceRegion(pxt, 0, 7, 3, 6);

	this.c6 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c6.sourceRegion(pxt, 4, 7, 3, 6);

	this.c7 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c7.sourceRegion(pxt, 8, 7, 3, 6);

	this.c8 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c8.sourceRegion(pxt, 12, 7, 3, 6);

	this.c9 = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.c9.sourceRegion(pxt, 16, 7, 3, 6);

	// "/" (slash) glyph
	this.cslash = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.cslash.sourceRegion(pxt, 20, 7, 3, 6);

	// "Moves" string
	this.moves = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.moves.sourceRegion(pxt, 0, 14, 21, 6);

	// "Level" string
	this.level = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.level.sourceRegion(pxt, 0, 21, 17, 6);

	// Heart glyph
	this.heart = new Displayable(texloc, texslotid, modelmatloc, texmatloc, projmatloc, viewmatloc);
	this.heart.sourceRegion(pxt, 25, 0, 7, 7);

}
