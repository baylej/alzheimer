// Card Object
function createCardMesh() {
	var vertices = [ // [8][3]
		-0.5, 0., -0.5,    0.5, 0., -0.5,    0.5, 0., 0.5,   -0.5, 0., 0.5,
		-0.5, 0., -0.5,    0.5, 0., -0.5,    0.5, 0., 0.5,   -0.5, 0., 0.5
	];

	var colors = [ // [8][3]
		.9, .9, .9,    .9, .9, .9,    .9, .9, .9,    .9, .9, .9,
		.9, .9, .9,    .9, .9, .9,    .9, .9, .9,    .9, .9, .9
	];

	var indices = [ // [4][3]
		 0,  2,  1,    0,  3,  2,
		 4,  5,  6,    4,  6,  7
	];

	var uvs = [ // [8][2]
		0., 1.,    1., 1.,    1., 0.,    0., 0.,
		0., 1.,    1., 1.,    1., 0.,    0., 0.
	];

	var hastx = [ // [8]
		0., 0., 0., 0.,    1., 1., 1., 1.
	];

	return new IndexedMesh(indices, vertices, colors, uvs, hastx);
}
Card.prototype.STATUSES = Object.freeze({NONE:0, REVERSED:1});
function Card(value, model_mat_uniform_loc, card_mesh, tex_trans_mat_uniform_loc, position_v3) {
	this.mesh = card_mesh;
	this.value = value;
	this.texmat_loc = tex_trans_mat_uniform_loc;
	this.position = position_v3;
	this.modelmat_loc = model_mat_uniform_loc;
	this.uv_trans_mat = mat3.identity(mat3.create());
	mat3.translate(this.uv_trans_mat, this.uv_trans_mat, vec2.fromValues(.5*(value&0x01), .5*((value&0x02)>>1)));
	mat3.scale(this.uv_trans_mat, this.uv_trans_mat, vec2.fromValues(.5, .5));
	// Show card animation
	this.animating = false;
	this.anim_start = 0.;
	this.status = this.STATUSES.NONE;
}
Card.prototype.display_position = vec3.fromValues(0., 2.2, 5.);
Card.prototype.reset = function() {
	this.animating = false;
	this.anim_start = 0.;
	this.status = this.STATUSES.NONE;
	this.animate = Card.prototype.animate;
}
Card.prototype.animate = function() {
	// Default animation function: card on table
	var model_mat = mat4.identity(mat4.create());
	mat4.translate(model_mat, model_mat, this.position);
	return model_mat;
}
Card.prototype.draw = function() {
	var model_mat = this.animate();
	gl.uniformMatrix4fv(this.modelmat_loc, gl.FALSE, model_mat);
	gl.uniformMatrix3fv(this.texmat_loc, gl.FALSE, this.uv_trans_mat);
	this.mesh.draw();
}
Card.prototype.triggerDisplayAnimation = function() {
	this.status = this.STATUSES.REVERSED;
	this.animating = true;
	this.anim_start = Date.now();
	this.animate = function() {
		var delta_t = Date.now() - this.anim_start;
		var model_mat = mat4.create();
		mat4.identity(model_mat);
		if (delta_t < 1000) {
			// go to display position and reverse the card
			var interp = delta_t/1000.;
			var rotX = -5/8 * Math.PI * interp;
			var q = quat.fromValues(Math.sin(rotX/2.), 0, 0, Math.cos(rotX/2.));
			var transl = vec3.clone(this.position);
			var to_display = vec3.subtract(vec3.create(), this.display_position, transl);
			vec3.scale(to_display, to_display, interp);
			vec3.add(transl, transl, to_display);
			mat4.fromRotationTranslation(model_mat, q, transl);
		}
		else if (delta_t < 2000) {
			// rest
			var rotX = -5/8 * Math.PI;
			var q = quat.fromValues(Math.sin(rotX/2.), 0, 0, Math.cos(rotX/2.));
			mat4.fromRotationTranslation(model_mat, q, this.display_position);
		}
		else if (delta_t < 3000) {
			// go back on the table
			var interp = (delta_t - 2000) /1000.;
			var rotX = -5/8 * Math.PI - 3/8 * Math.PI * interp;
			var q = quat.fromValues(Math.sin(rotX/2.), 0, 0, Math.cos(rotX/2.));
			var transl = vec3.clone(this.display_position);
			var from_display = vec3.subtract(vec3.create(), this.position, transl);
			vec3.scale(from_display, from_display, interp);
			vec3.add(transl, transl, from_display);
			mat4.fromRotationTranslation(model_mat, q, transl);
		}
		else {
			// reversed card on the table
			var rotX = -Math.PI;
			var q = quat.fromValues(Math.sin(rotX/2.), 0, 0, Math.cos(rotX/2.));
			mat4.fromRotationTranslation(model_mat, q, this.position);
			this.animating = false;
		}
		return model_mat;
	};
}
Card.prototype.triggerReverseAnimation = function() {
	this.status = this.STATUSES.NONE;
	this.animating = true;
	this.anim_start = Date.now();
	this.animate = function() {
		var delta_t = Date.now() - this.anim_start;
		var model_mat = mat4.create();
		mat4.identity(model_mat);
		var interp_tr = 0;
		var interp_ang = 0;
		if (delta_t < 500) {
			var interp_tr = delta_t/500.;
		}
		else if (delta_t < 1000) {
			var interp_tr = 1 - (delta_t-500)/500.;
		}
		else {
			delta_t = 1000;
			this.animate = Card.prototype.animate;
			this.animating = false;
		}
		if (delta_t > 250 && delta_t < 750) {
			interp_ang = (delta_t-250) / 500.;
		}
		else if(delta_t >= 750) {
			interp_ang = 1;
		}
		var rotZ = interp_ang * Math.PI;
		var rotX = -Math.PI;
		var qX = quat.fromValues(Math.sin(rotX/2.), 0, 0, Math.cos(rotX/2.));
		var qZ = quat.fromValues(0, 0, Math.sin(rotZ/2.), Math.cos(rotZ/2.));
		var q = quat.create(); quat.multiply(q, qX, qZ);
		var transl = vec3.clone(this.position);
		vec3.add(transl, transl, vec3.fromValues(0., interp_tr, 0.));
		mat4.fromRotationTranslation(model_mat, q, transl);
		return model_mat;
	};
}
