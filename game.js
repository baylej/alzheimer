// Check game context
function Check(game) {
	this.gamectx = game;

	this.exec = function() {
		GLOBAL_CONTEXT.context_stack.pop();
		if (this.gamectx.flipped1 == null || this.gamectx.flipped2 == null) {
			if (this.gamectx.movesCount > this.gamectx.maxmoves[this.gamectx.currentlevel]) {
				log('BOO you failed!');
				this.gamectx.fail();
			}
			return;
		}
		if (this.gamectx.cardset.values[this.gamectx.flipped1] != this.gamectx.cardset.values[this.gamectx.flipped2]) {
			// Invalid pair, flip both cards
			log('invalid pair, BOO');
			this.gamectx.cardset.cards[this.gamectx.flipped1].triggerReverseAnimation();
			this.gamectx.cardset.cards[this.gamectx.flipped2].triggerReverseAnimation();
			GLOBAL_CONTEXT.context_stack.push(
			  new Anim2(this.gamectx, this.gamectx.cardset.cards[this.gamectx.flipped1],
			                          this.gamectx.cardset.cards[this.gamectx.flipped2])
			);
		}
		else {
			// Valid pair
			log('valid pair, N1');
			this.gamectx.cardset.hasmatched[this.gamectx.flipped1] = true;
			this.gamectx.cardset.hasmatched[this.gamectx.flipped2] = true;
		}

		if (this.gamectx.cardset.allMatch()) {
			log('GG Mate!');
			this.gamectx.win();
		}
		else if (this.gamectx.movesCount > this.gamectx.maxmoves[this.gamectx.currentlevel]) {
			log('BOO you failed!');
			this.gamectx.fail();
		}
		this.gamectx.flipped1 = null;
		this.gamectx.flipped2 = null;
	};
}

// Animating a Card context
function Anim(game, card) {
	this.gamectx = game;
	this.animated = card;
	ev_stack.length=0;

	this.exec = function() {
		if (ev_stack.length > 0 || !this.animated.animating) {
			ev_stack.length=0;
			this.animated.anim_start = 0.;
			GLOBAL_CONTEXT.context_stack.pop();
		}
		this.gamectx.update();
	};
}

// Animating two Card contexts
function Anim2(game, card1, card2) {
	this.gamectx = game;
	this.anim1 = card1;
	this.anim2 = card2;
	ev_stack.length=0;

	this.exec = function() {
		if (ev_stack.length > 0 || (!this.anim1.animating && !this.anim1.animating)) {
			ev_stack.length=0;
			this.anim1.anim_start = this.anim2.anim_start = 0.;
			GLOBAL_CONTEXT.context_stack.pop();
		}
		this.gamectx.update();
	};
}

// A context that draws the game then calls od_ctx.exec()
function Overdraw(game_ctx, od_ctx) {
	this.gamectx = game_ctx;
	this.odctx = od_ctx;

	this.exec = function() {
		this.gamectx.update();
		this.odctx.exec();
	};
}

// Game context
function Game() {
	this.maxmoves  = [14, 12, 10, 8];

	this.currentlevel = 0;
	this.movesCount   = 0;
	this.lives = 3;

	// Active texture slot #1, generate and load texture to VRAM, bind texture uniform to slot #1
	gl.activeTexture(gl.TEXTURE1);
	var texture = generateMemoryTexture();

	// win texture to slot #2
	gl.activeTexture(gl.TEXTURE2);
	this.win_tex = loadTextureFromImage(win_tex_data);
	var dsp = new Displayable(GLOBAL_CONTEXT.tex_tex_loc, 2, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);
	this.win_disp = new TimedSkippableDisplayable(dsp, 2500, GLOBAL_CONTEXT.context_stack);

	// fail texture to slot #3
	gl.activeTexture(gl.TEXTURE3);
	this.fail_tex = loadTextureFromImage(fail_tex_data);
	dsp = new Displayable(GLOBAL_CONTEXT.tex_tex_loc, 3, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);
	this.fail_disp = new TimedSkippableDisplayable(dsp, 2500, GLOBAL_CONTEXT.context_stack);

	// game-over success texture to slot #4
	gl.activeTexture(gl.TEXTURE4);
	this.go_win_tex = loadTextureFromImage(gameover_win_d);
	dsp = new Displayable(GLOBAL_CONTEXT.tex_tex_loc, 4, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);
	this.go_win_disp = new TimedSkippableDisplayable(dsp, 3500, GLOBAL_CONTEXT.context_stack);

	// game-over failure texture to slot #5
	gl.activeTexture(gl.TEXTURE5);
	this.go_fail_tex = loadTextureFromImage(gamover_fail_d);
	dsp = new Displayable(GLOBAL_CONTEXT.tex_tex_loc, 5, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);
	this.go_fail_disp = new TimedSkippableDisplayable(dsp, 2500, GLOBAL_CONTEXT.context_stack);

	this.table = new Table(GLOBAL_CONTEXT.model_mat_loc);
	this.cardset = new CardSet(GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc);
	this.flipped1 = null;
	this.flipped2 = null;

	// sprite texture to slot #6
	gl.activeTexture(gl.TEXTURE6);
	this.sprite_tex = loadTextureFromImage(sheet_tex_data);
	this.spritesheet = new SpriteSheet(GLOBAL_CONTEXT.tex_tex_loc, 6, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);
	// UI class (UI elements in spritesheet)
	this.ui = new UI(this.spritesheet);

	// Interpret and react to user clicks
	this.click = function(x, y) {
		log('interpreting click at ' + x + ', ' + y);
		var clicked_card_id = this.cardset.getCardIdAt(x, y);
		if (clicked_card_id != null && !this.cardset.hasmatched[clicked_card_id] && clicked_card_id != this.flipped1) {
			if (this.flipped1 == null) {
				this.flipped1 = clicked_card_id;
			}
			else {
				this.flipped2 = clicked_card_id;
			}
			GLOBAL_CONTEXT.context_stack.push(new Check(this));
			this.cardset.cards[clicked_card_id].triggerDisplayAnimation();
			GLOBAL_CONTEXT.context_stack.push(new Anim(this, this.cardset.cards[clicked_card_id]));
			this.movesCount++;
		}
	};
	this.exec = function() {
		while (ev_stack.length > 0) {
			ev = ev_stack.pop();
			this.click(ev.x, ev.y);
		}
		this.update();
	};
}
// Resets the game to its current level's initial state
Game.prototype.reset = function() {
	this.movesCount = 0;
	this.cardset.reset();
	this.flipped1 = null;
	this.flipped2 = null;
}
// Resets the game to its initial state (after a gameover)
Game.prototype.restart = function() {
	this.currentlevel = 0;
	this.lives = 3;
	this.cardset.rehash();
	this.reset();
}
// Displays the failed screen then returns to the menu
Game.prototype.fail = function() {
	this.lives--;
	if (this.lives <= 0) { // Game Over ... n44b
		this.restart();
		GLOBAL_CONTEXT.context_stack.pop(); // return to menu
		GLOBAL_CONTEXT.context_stack.push(this.go_fail_disp); // after game-over screen
		this.go_fail_disp.reset();
	}
	else {
		this.reset();
		GLOBAL_CONTEXT.context_stack.push(this.fail_disp); // fail screen
		this.fail_disp.reset();
	}
}
// Displays the winning screen
Game.prototype.win = function() {
	this.currentlevel++;
	if (this.currentlevel == this.maxmoves.length) { // You won, GG
		this.restart();
		GLOBAL_CONTEXT.context_stack.pop(); // return to menu
		GLOBAL_CONTEXT.context_stack.push(this.go_win_disp); // after game-over screen
		this.go_win_disp.reset();
	}
	else {
		this.reset();
		this.cardset.rehash();
		GLOBAL_CONTEXT.context_stack.push(this.win_disp);
		this.win_disp.reset();
	}
}
Game.prototype.drawUI = function() {
	this.ui.drawLevel(this.currentlevel + 1);
	this.ui.drawMoves(this.movesCount, this.maxmoves[this.currentlevel]);
	this.ui.drawLives(this.lives);
}
Game.prototype.draw = function() {
	gl.uniform1i(GLOBAL_CONTEXT.tex_tex_loc, 1);
	GLOBAL_CONTEXT.camera.loadProjectionMatrix();
	GLOBAL_CONTEXT.camera.loadViewMatrix();
	this.table.draw();
	this.cardset.draw();
}
Game.prototype.update = function() {
	GLOBAL_CONTEXT.clearCanvas();
	this.draw();
	this.drawUI();
}


// MainMenu context
function MainMenu() {
	this.game = new Game();
	gl.activeTexture(gl.TEXTURE0);
	this.main_menu_tex = loadTextureFromImage(title_tex_data);
	this.displayable = new Displayable(GLOBAL_CONTEXT.tex_tex_loc, 0, GLOBAL_CONTEXT.model_mat_loc, GLOBAL_CONTEXT.tex_mat_loc, GLOBAL_CONTEXT.proj_mat_loc, GLOBAL_CONTEXT.view_mat_loc);

	this.exec = function() {
		if (ev_stack.length > 0) {
			log("menu -> newgame");
			ev_stack.length = 0;
			GLOBAL_CONTEXT.context_stack.push(this.game);
		}

		GLOBAL_CONTEXT.clearCanvas();
		this.game.draw();
		this.displayable.exec();
	};
}

// Contains the context stack of this JS animated application
// A context is a small object having an `exec(): void` method
// While being executed, a context may add/remove contexts from the stack

// Constructor is the entry point
function GlobalContext() {
	// Initialise the WebGL context
	gl.clearColor(0.21, 0.21, 0.21, 1.0);

	// Depth
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.clearDepth(1.0);

	// Culling
	gl.enable(gl.CULL_FACE);
	gl.frontFace(gl.CCW);

	// Blending
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	glCheckErr('init#1');

	// Shaders
	this.shaders = new Shaders();
	this.shaders.loadDefaultShaders();

	// GL names of uniform within shaders
	this.model_mat_loc = gl.getUniformLocation(this.shaders.defProgId, 'model_m4');
	this.tex_mat_loc   = gl.getUniformLocation(this.shaders.defProgId, 'tex_trans');
	this.tex_tex_loc   = gl.getUniformLocation(this.shaders.defProgId, 'tex');
	this.proj_mat_loc  = gl.getUniformLocation(this.shaders.defProgId, 'proj_m4');
	this.view_mat_loc  = gl.getUniformLocation(this.shaders.defProgId, 'view_m4');

	// Fixed position camera
	this.camera = new Camera(this.proj_mat_loc, this.view_mat_loc);
	this.camera.frustum(gl.drawingBufferWidth, gl.drawingBufferHeight, 0.1, 30.0, 45);
	this.camera.setPosition(0., -3., -7.);
	this.camera.setOrientation(Math.PI/8., 0., 0.);
	this.camera.foenum();
	glCheckErr('init#2');

	/// Context stack push and pop contexts to/from this stack
	this.context_stack = new Array();
}
GlobalContext.prototype.exec = function() {
	this.context_stack[this.context_stack.length-1].exec();
	glCheckErr('exec');
}
GlobalContext.prototype.clearCanvas = function() {
	gl.clear(gl.DEPTH_BUFFER_BIT);
	gl.clear(gl.COLOR_BUFFER_BIT);
}
// Singleton, do not create new instances.
var GLOBAL_CONTEXT = null;

if (gl != null) {
	log('Launching ...');
	try {
		GLOBAL_CONTEXT = new GlobalContext();
		// Initial context
		GLOBAL_CONTEXT.context_stack.push(new MainMenu());
		var loop = function() {
			GLOBAL_CONTEXT.exec();
			requestAnimationFrame(loop); // loops on loop();
		};
		loop();
	}
	catch (msg) {
		log(msg);
	}
}
