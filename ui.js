
// User interace
function UI(spritesheet) {
	this.sprites = spritesheet;

	// set constant target location
	this.sprites.level.targetScaled(pxs,-32+8.5, 28, 17, 6);
	this.sprites.cslash.targetScaled(pxs,1.5, 21, 3, 6);
	this.sprites.moves.targetScaled(pxs,-32+10.5, 21, 21, 6);
}

// returns the Displayable to draw the given number [0-9]
UI.prototype.getChar = function(number) {
	switch(number) {
		case 0: return this.sprites.c0;
		case 1: return this.sprites.c1;
		case 2: return this.sprites.c2;
		case 3: return this.sprites.c3;
		case 4: return this.sprites.c4;
		case 5: return this.sprites.c5;
		case 6: return this.sprites.c6;
		case 7: return this.sprites.c7;
		case 8: return this.sprites.c8;
		case 9: return this.sprites.c9;
		default: log("Error: NaN " + number);
	}
	return this.c0;
}

UI.prototype.drawLevel = function(level) {
	this.sprites.level.exec();
	var lvlc = this.getChar(level);
	lvlc.targetScaled(pxs,-8+1.5, 28, 3, 6);
	lvlc.exec();
}

UI.prototype.drawMoves = function(moves, onmoves) {
	this.sprites.moves.exec();
	var mvc = this.getChar(Math.floor(moves/10));
	mvc.targetScaled(pxs,-8+1.5, 21, 3, 6);
	mvc.exec();
	mvc = this.getChar(moves%10);
	mvc.targetScaled(pxs,-4+1.5, 21, 3, 6);
	mvc.exec();

	this.sprites.cslash.exec();

	mvc = this.getChar(Math.floor(onmoves/10));
	mvc.targetScaled(pxs,4+1.5, 21, 3, 6);
	mvc.exec();
	mvc = this.getChar(onmoves%10);
	mvc.targetScaled(pxs,8+1.5, 21, 3, 6);
	mvc.exec();
}

UI.prototype.drawLives = function(lives) {
	switch (lives) {
		case 3:
			this.sprites.heart.targetScaled(pxs,32 - 7 + 3.5, 12.5, 7, 7);
			this.sprites.heart.exec();
		case 2:
			this.sprites.heart.targetScaled(pxs,32 - 7 + 3.5, 20.5, 7, 7);
			this.sprites.heart.exec();
		case 1:
			this.sprites.heart.targetScaled(pxs,32 - 7 + 3.5, 28.5, 7, 7);
			this.sprites.heart.exec();
	}
}
