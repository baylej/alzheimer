// Table Object
function Table(model_mat_uniform_loc) {
	var vertices = [ // [4][3]
		-0.5, 0., -0.5,    0.5, 0., -0.5,    0.5, 0., 0.5,   -0.5, 0., 0.5
	];

	var colors = [ // [4][3]
		.45, .3, .1,  .45, .3, .1,  .45, .3, .1,  .45, .3, .1,
	];

	var indices = [ // [2][3]
		 0,  2,  1,   0,  3,  2
	];

	this.mesh = new IndexedMesh(indices, vertices, colors);
	this.modelmat = mat4.create();
	mat4.identity(this.modelmat);
	mat4.scale(this.modelmat, this.modelmat, vec3.fromValues(5.,5.,7.));
	this.modelmat_loc = model_mat_uniform_loc;
}
Table.prototype.draw = function() {
	gl.uniformMatrix4fv(this.modelmat_loc, gl.FALSE, this.modelmat);
	this.mesh.draw();
}
