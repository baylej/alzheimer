# Alzheimer

---

A 3D lowres memory game made with HTML5+JS+WebGL for [#LOWREZJAM2k16](https://itch.io/jam/lowrezjam2016).

[Play here](https://dl.dropboxusercontent.com/u/806030/Alzheimer/index.html),
