// Generates a 16x16px image containing 4 4x4px images, load it in the VRAM
function generateMemoryTexture() {
	var cols = [
		new Uint8Array([  0,  10, 120]),
		new Uint8Array([  0, 140, 200]),
		new Uint8Array([220, 110,   0]),
		new Uint8Array([255, 200,  90])
	];

	var pxmp = new Uint8Array(256 * 3); // 16*16 = 256
	pxmp.fill(255);
	for (var i=0; i<256*3; i+=3+3*Math.ceil(4*Math.random())) {
		var y = Math.floor(i / (16. * 3.));
		var x = (i - 16*3 * y) / 3.;
		var col_id =  (Math.floor(x/8.)<<1) + Math.floor(y/8.);
		pxmp.set(cols[col_id], i);
	}

	var tex = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, tex);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, 16, 16, 0, gl.RGB, gl.UNSIGNED_BYTE, pxmp);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	return tex;
}

function modernFisherYatesShuffle(a) {
	for (var i=a.length-1; i>=1; i--) {
		var j = Math.floor(Math.random()*(i+1));
		var tmp = a[j];
		a[j] = a[i];
		a[i] = tmp;
	}
}

// A card set contains 8 card and all the logic
function CardSet(model_mat_uniform_loc, tex_trans_mat_uniform_loc) {
	this.modelmatloc = model_mat_uniform_loc;
	this.texmatloc   = tex_trans_mat_uniform_loc;
	this.cards = [];
	this.values = [0, 1, 2, 3,   0, 1, 2, 3];
	this.hasmatched = [false, false, false, false,   false, false, false, false];
	this.card_mesh = createCardMesh();
	this.rehash();
}
// Returns all cards and reset all cards to their initial states
CardSet.prototype.reset = function() {
	this.hasmatched.fill(false);
	for (var it=0; it<8 ; it++) {
		this.cards[it].reset();
	}
	log('Reset cardset');
}
CardSet.prototype.rehash = function() {
	this.hasmatched.fill(false);
	modernFisherYatesShuffle(this.values);
	for (var it=0; it<8 ; it++) {
		var xtr = 1.5; if (it>=4) xtr = -xtr;
		var ztr = (it%4) * 1.5 - 2.;
		var pos = vec3.fromValues(xtr, .05, ztr);
		this.cards[it] = new Card(this.values[it], this.modelmatloc, this.card_mesh, this.texmatloc, pos);
	}
	log('Rehashed cardset');
}
CardSet.prototype.draw = function() {
	for (var it=0; it<8 ; it++) {
		this.cards[it].draw();
	}
}
CardSet.prototype.getCardIdAt = function(x, y) {
	if (y>200 && y<225) {
		if (x>120 && x<200) {
			return 4;
		}
		else if (x>320 && x<400) {
			return 0;
		}
	}
	else if (y>235 && y<265) {
		if (x>100 && x<185) {
			return 5;
		}
		else if (x>330 && x<420) {
			return 1;
		}
	}
	else if (y>275 && y<320) {
		if (x>55 && x<170) {
			return 6;
		}
		else if (x>340 && x<445) {
			return 2;
		}
	}
	else if (y>345 && y<415) {
		if (x>10 && x<150) {
			return 7;
		}
		else if (x>370 && x<505) {
			return 3;
		}
	}
	return null;
}
// Returns true if all cards are matched
CardSet.prototype.allMatch = function() {
	var res =
		this.hasmatched[0] && this.hasmatched[4] &&
		this.hasmatched[1] && this.hasmatched[5] &&
		this.hasmatched[2] && this.hasmatched[6] &&
		this.hasmatched[3] && this.hasmatched[7];
	return res;
}
